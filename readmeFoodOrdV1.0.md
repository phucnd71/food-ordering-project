## Food Ordering
## Description
### ** Home cửa hàng Foot Ordering, xây dựng trang bán hàng, load data bằng jquery, Sửa một đơn hàng, Xóa 1 đơn hàng, loc pizza
## Feature
+ Xây dựng header section
![XayDung](images/01_img_headersection.png)
+ Xây dựng, load dữ liệu pizza bằng jquery
![XayDung](images/02_img_kichensection.png)
+ Xây dựng, load dữ liệu blog bằng jquery
![XayDung](images/03_img_blogsection.png)
+ Xây dựng Contact Us Section and Footer
![XayDung](images/04_img_contact_us_section_and_footer.png)
+ Thêm mới một đơn hàng
![Taomoi](images/05_them_moi_mot_pizza.png)
+ Chi tiết đơn hàng
![Detail](images/06_chi_tiet_don_hang.png)
+ Phương thức thanh toán
![Payment](images/07_phuong_thuc_thanh_toan.png)
+ Thông báo thêm mới thành công
![Success](images/08_them_moi_thanh_cong.png)
+ Sửa một đơn hàng
![Update](images/09_sua_mot__order.png)
+ Xóa một đơn hàng
![Delete](images/10_xoa_mot_order.png)
+ Loc pizza
![Filter](images/11_loc_pizza.png)

## Technology
+ Front-end:
  :1.[Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-stated/introduction/)
  :2.Javascript
  :3.Jquery 3
  :4.Ajax
  :5.Local Storage
  :6.JSON
+ Back-end:
 : Spring boot with Hibernate
+ KIT:
 : [AdminLTE](https://adminlte.io/)
+ DB:
 : MySQL - PhpMyAdmin